if(document.getElementsByClassName == undefined) { 
   document.getElementsByClassName = function(cl) { 
      var retnode = []; 
      var myclass = new RegExp('\\b'+cl+'\\b'); 
      var elem = this.getElementsByTagName('*'); 
      for (var i = 0; i < elem.length; i++) { 
         var classes = elem[i].className; 
         if (myclass.test(classes)) { 
            retnode.push(elem[i]); 
         } 
      } 
      return retnode; 
   } 
}; 

window.onload = function(){

    var arRadioNames = ['adultCount', 'childrenCount', 'babysCount'];
    for (var i=0; i < arRadioNames.length; i++) {
        var inputName = arRadioNames[i];
        var peopleCount = document.getElementsByName(inputName);
        for (var j=0; j < peopleCount.length; j++) {
          peopleCount[j].onchange = function () {
            var elVal = document.getElementsByClassName('js-' + this.getAttribute('name') + 'Value');
            for (var i=0; i < elVal.length; i++) {
                elVal[i].innerHTML = this.getAttribute('value');
            }
          }
        }
    }
};
